from flask import Flask, jsonify, request, render_template
from flask_sqlalchemy import SQLAlchemy
import marshmallow
from flask_marshmallow import Marshmallow
from flask import Response
from flask_cors import CORS



db = SQLAlchemy()
app = Flask(__name__)
CORS(app, supports_credentials=True)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///sqlite.db"
db.init_app(app)

marsh = Marshmallow(app)



# Model and serializer
class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    active = db.Column(db.Boolean, default=False)

class TaskSchema(marsh.Schema):
    class Meta:
        fields = ('id','name','active')

Task_Schema = TaskSchema(many=False)
Tasks_Schema = TaskSchema(many=True) 




with app.app_context():
    db.create_all()


@app.route("/task/<int:id>" , methods=["GET","POST","PUT","DELETE"])
@app.route("/task/" , methods=["GET","POST","PUT","DELETE"])
def task(id=None):
    if request.method == "POST":
        instance = Task(
            name=request.json["name"],
        )
        db.session.add(instance)
        db.session.commit()
        res,status = Task_Schema.dump(instance),200
    elif request.method == "PUT":
        status = request.json['status']
        Text = request.json['name']
        instance = db.get_or_404(Task, id)
        if instance:
            instance.active = status
            if Text :
                instance.name = Text
            db.session.commit()
            res,status = Task_Schema.dump(instance),200
    elif request.method == "DELETE":
        instance = db.get_or_404(Task, id)
        if instance:
            db.session.delete(instance)
            db.session.commit()
            res,status = {"msg":"Deleted Successfully"},201
    elif request.method == "GET":
        if id:
            instance = db.get_or_404(Task, id)
            res,status = Task_Schema.dump(instance),200
        else:
            instance = Task.query.filter().all()
            res,status = Tasks_Schema.dump(instance),200
    return jsonify(res),status



if __name__ == "__main__":
    app.run(debug=True)