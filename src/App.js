import Navbar from "./component/Navbar";
import AddTask from "./component/Form"
import { Provider } from "react-redux";
import { configureStore } from '@reduxjs/toolkit'
import ReducerFunc from './component/reducer'
import React from "react";

export const HOST = "http://127.0.0.1:5000/task"

export const Store = configureStore({ reducer: ReducerFunc })

export default function App() {

  React.useEffect(() => {
    fetch(HOST)
      .then(res => res.json())
      .then(data => {
        Store.dispatch({ type: "intialValue", payload: data })
      })
  });


  return (
    <Provider store={Store}>
      <div className="App" >
        <Navbar/>
        <AddTask/>
      </div>
    </Provider>
  );
}

