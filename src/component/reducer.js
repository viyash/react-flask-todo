import { createAction } from '@reduxjs/toolkit'
import {HOST} from '../App'

export const addToList = createAction('addToList')
export const checkbox = createAction('checkbox')
export const deleteTask = createAction('deleteTask')
export const initialValue = createAction('initialValue')
export const updateInput = createAction('updateInput')


const initialState = {
    name: '',
    listArr: []
}

export const fetchAddToList = (name) => (dispatch) => {
  fetch(`${HOST}/`, {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ name: name })
  })
    .then(res => res.json())
    .then(data => {
      dispatch(addToList(data))
    })
}

export const fetchCheckbox = (id, status,name=null,e=null) => (dispatch) => {

  fetch(`${HOST}/${id}`, {
    method: 'PUT',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ status: status,name:name })
  })
    .then(res => res.json())
    .then(data => {
      dispatch(checkbox(data))
      if(e){
        e.target.contentEditable = false
      }
    })
}

export const fetchDeleteTask = (id) => (dispatch) => {
  fetch(`${HOST}/${id}`, {
    method: 'DELETE',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then(res => res.json())
    .then(data => {
      dispatch(deleteTask(id))
    })
}


function ReducerFunc(state = initialState, action) {
  switch (action.type) {
    case 'addToList':
      return { ...state, listArr: [...state.listArr,action.payload], name: '' }
    case 'checkbox':
      return { ...state,name:'' }
    case 'deleteTask':
      const arr = state.listArr.filter(element => {
        return element.id !== parseInt(action.payload)
      })
      return { ...state, listArr: arr }
    case 'intialValue':
      return { ...state, listArr: action.payload }
    case 'updateinput':
      return { ...state, name: action.payload }
    default:
      const updatedArr = state.listArr.reduce((acc, element) => {
        if (action.payload.key === element.id) {
          acc.push({ ...element, name: action.payload.value })
        } else {
          acc.push(element)
        }
        return acc
      }, [])
      return { ...state, listArr: updatedArr }
  }
}


export default ReducerFunc