import React, { Component } from "react";
import { Store } from '../App'
import "../css/form.css"
import { fetchAddToList, fetchCheckbox, fetchDeleteTask } from './reducer.js'


class AddTask extends Component {

    constructor(props) {
        super(props)
        this.state = Store.getState();
    }

    componentDidMount() {
        Store.subscribe(() => {
            this.setState(Store.getState());
        });
    }

    addToList = (e) => {
        if (!e.target.parentElement.querySelector('input').value) return
        Store.dispatch(fetchAddToList(e.target.parentElement.querySelector('input').value))
    }

    deleteTask = (e) => {
        Store.dispatch(fetchDeleteTask(e.target.getAttribute('value')))
    }

    updateCheckBox = (e) => {
        Store.dispatch(fetchCheckbox(e.target.getAttribute('value'), e.target.checked))
    }

    updateState = (e) => {
        Store.dispatch({ type: "updateinput", payload: e.target.value })
    }

    updatebox = (e) => {
        e.target.contentEditable = true
    }

    updateBoxData = (e) =>{
        e.target.contentEditable = false
        Store.dispatch(fetchCheckbox(e.target.getAttribute('value'), e.target.parentElement.querySelector('input').checked,e.target.innerText,e))
    }

    render() {
        return (
            <div className="main-box">
                <div className="buttonIn">
                    <input type="text" className="shadow" placeholder="You know you should..." id="addtask" value={this.state.name} onChange={this.updateState} />
                    <button id="clear" onClick={this.addToList}>+</button>
                </div>
                {this.state.listArr.length > 0 ? <ol className="Task-list list-group list-group-numbered container p-5 listStyle">
                    {
                        this.state.listArr.map((element) => {
                            return (
                                <li key={element.id}  >
                                    <div style={{ display: "flex", justifyContent: "space-between" }} className="list-card list-group-item shadow p-3 m-2" key={element.id} value={element.id} >
                                        <div style={{ display: "flex", flexWrap: "wrap", wordBreak: "break-all" }}>
                                            <input type="checkbox" style={{ margin: "10px" }} value={element.id} onChange={this.updateCheckBox} defaultChecked={element.active ? true : false} />
                                            <span onBlur={this.updateBoxData} value={element.id} onDoubleClick={this.updatebox}>{element.name}</span>
                                        </div>
                                        <img className="delete-icon" alt="del" value={element.id} onClick={this.deleteTask} src="https://img.icons8.com/ios-glyphs/30/B70707/filled-trash.png" />
                                    </div>
                                </li>
                            )
                        })

                    }
                </ol>
                    : null}

            </div>

        );
    }
}

export default AddTask;